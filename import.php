<?php

$dbHost = "ds049848.mongolab.com:49848";
$dbName = "ibmavnet_phase2";
$dbUser = "import";
$dbPass = "qwerty";
$importDir = "stage_db_mongolab_07_07_2015";

$collections = array(
	"RefreshToken",
	"file",
	"oauthAccessToken",
	"oauthClient",
	"object.catalog",
	"object.category",
	"object.contact",
	"object.forgotten_password",
	"object.partner",
	"object.product",
	"object.promotion",
	"object.resource",
	"object.social",
	"object.solution",
	"object.tagcategory",
	"object.vendor",
	"object.video",
);

$collections = array("user");

foreach ($collections as $c) {
	echo "Importing '{$c}' to database '{$dbName}'\n";
	echo "mongoimport --host {$dbHost} --db {$dbName} --collection {$c} ./{$importDir}/{$c}.json --username {$dbUser} --password {$dbPass} --jsonArray\n";
	exec("mongoimport --host {$dbHost} --db {$dbName} --collection {$c} ./{$importDir}/{$c}.json --username {$dbUser} --password {$dbPass} --jsonArray");
	echo "Done\n";
}
