<?php

$dbHost = "c899.candidate.36.mongolayer.com:10899";
$dbName = "channel-tools-mongodb";
$dbUser = "export";
$dbPass = "qwerty";
$exportDir = "production_db_compose_10_07_2015";

$collections = array(
	"RefreshToken",
	"file",
	"oauthAccessToken",
	"oauthClient",
	"object.catalog",
	"object.category",
	"object.contact",
	"object.forgotten_password",
	"object.partner",
	"object.product",
	"object.promotion",
	"object.resource",
	"object.social",
	"object.solution",
	"object.tagcategory",
	"object.vendor",
	"object.video",
	"user"
);

//$collections = array("user");

foreach ($collections as $c) {
	echo "Exporting '{$c}' to '{$exportDir}'\n";
	exec("mongoexport --host {$dbHost} --db {$dbName} --collection {$c} --username {$dbUser} --password {$dbPass} --out ./{$exportDir}/{$c}.json");
	echo "Done\n".str_repeat("-", 100)."\n\n";
}
